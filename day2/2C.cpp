#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <string>
#include <set>
#define ulli unsigned long long int
using namespace std;

ulli first,last;

void g( ulli x )
{
	last = x%10;
	for(;x>=10;x/=10);
	first = x;
}

ulli f( ulli x )
{
	if( x<10 )
		return x;
	g(x);

	return 9+x/10-(first>last);
}

int main()
{
	ulli l,r;
	cin >> l >> r;
	cout << ( f(r)-f(l-1) ) << endl;
    return 0;
}
