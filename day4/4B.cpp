#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <string>
#include <set>
#include <algorithm>
#define ulli unsigned long long int
using namespace std;

int n,m,d,t;
ulli sum,result;
vector<int> v;

ulli abs( ulli x )
{
	if( x<0 )
		return -x;
	return x;
}

int main()
{
	cin >> n >> m >> d;
	
	for(int i=0;i<n*m;i++)
	{
			cin >> t;
			v.push_back(t);
	}
	
	sort(v.begin(),v.end());

	t = v[ v.size()/2 ];

	for(int i=0;i<n*m;i++)
	{
		if( abs( v[i]-t ) % d )
		{
			cout << -1 << endl;
			return 0;
		}
		else
			result += abs( v[i]-t )/d;
	}
	cout << result<< endl;
	

    return 0;
}
