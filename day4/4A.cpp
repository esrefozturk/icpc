#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <string>
#include <set>
#define ulli unsigned long long int
using namespace std;
ulli n,k,a,b,sum=0;
int main()
{
	cin >> n >> k;
	for(int i=0;i<n;i++)
	{
		cin >> a >> b;
		sum += b-a+1;
	}
	if( sum%k )
		cout << (k-sum%k  ) << endl;
    else
		cout << 0 << endl;
	return 0;
}
