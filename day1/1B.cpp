#include <iostream>
#include <cstdio>
#include <cstdlib>
#define ulli unsigned long long int
using namespace std;
ulli ar[10];

bool f( ulli i  )
{
    for( ;i;i/=10 )
    {
        if( ar[ i%10 ]) return true;
    }
	return false;

}

int main()
{
    ulli x,d=0;
    cin >> x;
    for( ulli y=x;y;y/=10 )
    {
        ar[ y%10 ] = 1;
    }
    for(ulli i=1;i*i<=x;i++)
    {
        if( x%i==0 )
        {
            if( f(i)  )
                d++;
            if( x/i !=i && f(x/i)  )
                d++;
        }
    }
    cout << d << endl;
    return 0;
}
